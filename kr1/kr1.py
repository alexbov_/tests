from sys import stdin as sys_stdin

result: float = 42

for line in sys_stdin:
    txtline = line.rstrip()
    if all(char.isdigit() for char in txtline) is True and txtline != "":
        result += 1 / int(txtline)

print("Сумма ряда: {0:.9f}".format(result))
