from copy import deepcopy
def gauss(rows_count, matrix):
    # Прямой ход метода Гаусса
    # Смысл метода: Последовательно исключаем переменную за переменной, пока в одной из строк не будет однозначно определена переменная xi.
    for i in range(rows_count):
        for j in range(rows_count):
            if i != j:
                ratio = matrix[j][i] / matrix[i][i]
                for k in range(rows_count+1):
                    # Заменяем
                    matrix[j][k] = matrix[j][k] - ratio * matrix[i][k]


    result_vector = []
    # Обратный ход метода Гаусса.
    for i in range(rows_count):
        vector_coord = round(matrix[i][rows_count] / matrix[i][i], 2)
        result_vector.append(vector_coord)

    return result_vector


def calculation(rows_count, matrix):
    # Если количество строк в матрице = 1, определитель равен первой ячейке матрицы
    if rows_count == 1:
        return matrix[0][0]
    # Если же количество строк в матрице = 2, то определитель вычисляется по формуле.
    elif rows_count == 2:
        return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1]
    # Иначе определитель высчитывается рекурсивно через определители более маленьких матриц
    else:
        determinant = 0
        for i in range(rows_count):
            new_matrix = deepcopy(matrix)
            new_matrix.pop(0)
            for j in range(rows_count - 1):
                new_matrix[j].pop(i)
            determinant += (
                    (-1) ** i * matrix[0][i] * calculation(rows_count=rows_count - 1, matrix=new_matrix)
            )
        return determinant # Выводим определитель


def cramer(rows_count, matrix):
    # Находим определитель matrixdeterminant исходной матрицы marix.
    matrixdeterminant = determinantfunc(matrix)
    if matrixdeterminant == 0:  # Нельзя решить
        raise ValueError

    result_vector = []
    for i in range(rows_count):# Считаем определители
        line = deepcopy(matrix)
        for j in range(rows_count):  # Заменяем значения в матрице на свободные коэффициенты.
            line[j][i] = matrix[j][-1]
        # Вычисляем определитель матрицы со свободными коэффициентами и делим на определитель матрицы.
        line_determinant = round(determinantfunc(matrix=line) / matrixdeterminant, 2)
        result_vector.append(line_determinant)

    return result_vector # Выводим результат

def determinantfunc(matrix):

    # Создаю трехмерную матрицу через операции со строками
    # Произведение диагональных элементов является определителем
    # Matrix - Матрица для нахождения определителя

    n = len(matrix)
    new_matrix = copy_matrix(matrix)   # Копируем матрицу

    for fd in range(n):  # Диагональ фокуса
        if new_matrix[fd][fd] == 0:
            new_matrix[fd][fd] = 1.0e-18  # Добавляем ноль
        for i in range(fd+1, n):  # Пропускаем строки
            realrow = new_matrix[i][fd] / new_matrix[fd][fd]  # RealRow - текущий ряд
            for j in range(n):
                new_matrix[i][j] = new_matrix[i][j] - realrow * new_matrix[fd][j]

    product = 1.0
    for i in range(n):
        product *= new_matrix[i][i]  # Произведение диагоналей является определителем.

    return product # определитель матрицы

def copy_matrix(matrix):

    # Создаем и возвращаем копию матрицы
    # Matrix Матрица для копирования

    # Получаем размерность матрицы
    rows = len(matrix)
    cols = len(matrix[0])

    # Создаем матрицу нулей
    new_matrix = zeros_matrix(rows, cols)

    # Копируем значения матрицы в копию матрицы
    for i in range(rows):
        for j in range(cols):
            new_matrix[i][j] = matrix[i][j]

    return new_matrix # Копия данной матрицы

def zeros_matrix(rows, cols):

    # Создаем матрицу с нулями, Rows - количество строк, которые должна иметь матрица.
    # Сols - количество столбцов, которые должна иметь матрица

    matrix = []
    while len(matrix) < rows:
        matrix.append([])
        while len(matrix[-1]) < cols:
            matrix[-1].append(0.0)

    return matrix # Получаем список списков, образующих матрицу


