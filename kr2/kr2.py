from os import name as os_name, system as os_system
from time import sleep as time_sleep
from sys import stdin as sys_stdin
from ctypes import windll

STD_OUTPUT_HANDLE = -11
h = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)

os_system("cls" if os_name == "nt" else "clear")  # Чтоы не отображался Unicode, очищаю систему
earthall = []  # Все кадры
earthone = []  # Один кадр
earthreading = False  # Считывание
for line_raw in sys_stdin:
    line = line_raw.rstrip("\n")
    if line.startswith("```") == 1:
        if earthreading is False:  # Условие начала считывания.
            earthreading = True
        else: # Условие окончания считывания
            earthall.append(earthone.copy())  # Копии кадров в массив
            earthreading = False
            earthone = []

        continue

    if earthreading is True: # Добавление строки кадра.
        earthone.append(line)


try:
    while True:
        for earthone in earthall:
            for earthone_line in earthone:
                print("\u001b[31m", end="")  # Красим цвет текста в консоли
                print(earthone_line)
            windll.kernel32.SetConsoleCursorPosition(h, 0, 0)
            time_sleep(0.4)
            os_system("cls" if os_name == "nt" else "clear")
except KeyboardInterrupt:
    pass
