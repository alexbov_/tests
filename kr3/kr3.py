from lib import saxpy

# Определяем тип входных данных, если тип неверный - выдаем ошибку.
choice = input("int или float?\t").lower()
if "int" in choice:
    input_type = int
elif "float" in choice:
    input_type = float
else:
    raise TypeError

x = list(map(input_type, input("Координаты X:\t").split()))
y = list(map(input_type, input("Координаты вектора Y:\t").split()))
a = input_type(input_type(input("Угол <A:\t")))

saxpyvector = saxpy(x, y, a)
print(f"Результат - {str(saxpyvector)} ")