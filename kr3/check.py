import time
import random
from lib import saxpy


print("Начинаем вычисления..")
timeinteger = []  # Время вычисления к integer
for n in range(int(10e5), int(10e6), int(10e5)):
    time_temp = 0
    x = [random.randint(-50, 50) for point in range(n)]  # Генерируем числа для координат x
    y = [random.randint(-50, 50) for point in range(n)]  # Генерируем числа для координат y
    a = random.randint(-50, 50) # Генерируем скаляр а
    for l in range(3):  # Создаем проход
        l = l + 0  # Используем эту переменную только для цикла
        start = time.time()  # Начало выполнения алгоритма
        saxpyalg = saxpy(x, y, a)   # Используем алгоритм SAXPY
        end = time.time()   # Конец выполнения алгоритма
        time_temp += end - start
    timeinteger.append(time_temp / 3)
    str(timeinteger) # Конвентируем в строку
    file = open("timeinteger.txt", 'a')  # Создаем файл, разрешена дозапись в файл
    file.write(f'{timeinteger[-1]:.8f}\n')  # Записываем время в файл

timefloat = []  # Время вычисления к float

for n in range(int(10e5), int(10e6), int(10e5)):
    time_temp = 0
    x = [random.uniform(-50, 50) for point in range(n)]
    y = [random.uniform(-50, 50) for point in range(n)]
    a = random.uniform(-50, 50)
    for p in range(3):  # Создаем проход
        p = p + 0  # Используем эту переменную только для цикла
        start = time.time()  # Начало выполнения алгоритма
        saxpyalg = saxpy(x, y, a)  # Используем алгоритм SAXPY
        end = time.time()  # Конец выполнения алгоритма
        time_temp += end - start
    timefloat.append(time_temp / 3) # Добавляем в список время работы float
    file = open("timefloat.txt", 'a') # Создаем файл, разрешена дозапись в файл
    file.write(f'{timefloat[-1]:.8f}\n') # Записываем время в файл

print("Готово!")

