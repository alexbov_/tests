import time
import lib
from random import randint, uniform

n_min = 50
n_max = 501
n_step = 50
time_data = []

choice = input("int или float: ")
if choice == "int":
    for n in range(int(n_min), int(n_max), int(n_step)):
        amatrix = [[randint(-10, 10) for j in range(n)] for i in range(n)]
        bmatrix = [[randint(-10, 10) for j in range(n)] for i in range(n)]
        averages = []
        for _ in range(3):
            start = time.time()
            lib.matrix(amatrix, bmatrix)
            averages.append(time.time() - start)
            time_result = time.time() - start
            print(f'Заняло времени для матрицы с размерностью {n_min}: {time_result}')

        time_data.append(sum(averages) / 3)
        file = open("timeinteger.txt", 'a')  # Создаем файл, разрешена дозапись в файл
        file.write(f'{time_data[-1]:.8f}\n')  # Записываем время в файл
        print(f'Среднее значение: {time_data[-1]}')
        n_min += n_step

elif choice == "float":

    for n in range(int(n_min), int(n_max), int(n_step)):
        amatrix = [[uniform(-10, 10) for j in range(n)] for i in range(n)]
        bmatrix = [[uniform(-10, 10) for j in range(n)] for i in range(n)]
        averages = []

        for _ in range(3):
            start = time.time()
            lib.matrix(amatrix, bmatrix)
            averages.append(time.time() - start)
            time_result = time.time() - start
            print(f'Заняло времени для матрицы с размерностью {n_min}: {time_result}')

        time_data.append(sum(averages) / 3)
        file = open("timefloat.txt", 'a')  # Создаем файл, разрешена дозапись в файл
        file.write(f'{time_data[-1]:.8f}\n')  # Записываем время в файл
        print(f'Среднее значение: {time_data[-1]}')
        n_min += n_step
file.close()